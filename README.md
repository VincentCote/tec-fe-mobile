# TEC's frontend 2018 redesign

This repo features a modern front-end development stack, to make sure we move forwards at a rapid pace.

- Pug
- LESS
- ES6 w/ Babel
- Browsersync

## To develop:

1. Install [Node.js](https://nodejs.org), if not already installed
2. Clone the repo, and install the required packages using [Yarn](https://yarnpkg.com).
	- `yarn`
3. Install `gulp-cli` globally (if not already installed)
	- `yarn global add gulp-cli`
4. Start Browsersync
	- `gulp serve`

For a complete list of all the gulp tasks, please see `gulpfile.babel.js` in the root of the repo. The webpack config is also at the root: `webpack.config.babel.js`.

## To compile for production

1. Clone the repo, and install the required packages (see above).
2. Install `gulp-cli` globally (if not already installed, see above)
3. Run `gulp compile --release`

**Important:**  
If you plan to integrate the content of this repository in the current (old) website, please make sure to compile using the `--patch` switch as it will include the `components/_patch.less` file which contains the proper style overrides to make it work over there.
