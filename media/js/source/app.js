import Megamenu from './components/megamenu';
import StickyTopbar from './components/sticky-topbar';

export default class App {
	static bootstrap() {
		this.megamenu = new Megamenu();
		this.stickyTopbar = new StickyTopbar();
	}
}

App.bootstrap();
