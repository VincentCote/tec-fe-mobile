export default class StickyTopbar {
	constructor() {
		this.topBarHeight = document.querySelector('.js-topbar-container').clientHeight;
		this.observerCallback = this.observerCallback.bind(this);

		if (document.querySelector('.js-topbar') !== null) {
			this.observerCallback = this.observerCallback.bind(this);
			this.observer = new IntersectionObserver(this.observerCallback, {threshold: 1});
			this.observer.observe(document.querySelector('.js-topbar'));
		}
	}

	observerCallback(entries) {
		const topbar = document.querySelector('.js-topbar-container');
		entries.forEach(entry => {
			entry.target.style.height = (entry.boundingClientRect.height > 0) ? `${this.topBarHeight}px` : '';
			if (entry.boundingClientRect.y >= 0) {
				topbar.style.position = '';
				entry.target.style.height = '';
			} else {
				topbar.style.position = 'fixed';
				entry.target.style.height = `${this.topBarHeight}`;
			}
		});
	}
}
