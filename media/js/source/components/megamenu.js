import Utils from '../utils/utils';
import { underline } from 'ansi-colors';

export default class Megamenu {
	constructor() {
		this.changeMenuPage = this.changeMenuPage.bind(this);

		this.triggers = new Map();
		this.pages = new Map();

		var x = document.getElementsByClassName('fas fa-search')[0];

		// Searchbar-related function in the mobile menu  
		x.addEventListener('click', function () {
			var cssValue = document.getElementsByName('nav_right_search')[0];

			if (cssValue.type == 'text') {
				cssValue.type = 'hidden';
			}
			else {
				cssValue.type = 'text';
			}
		});

		// Switch categories (display a light blue background for the selected sub-menu)
		[...document.querySelectorAll('.megamenu__sidebar__link')].forEach(category => {
			category.addEventListener('click', function () {
				if (!category.classList.contains('megamenu__sidebar__link--active')) {
					[...document.querySelectorAll('.megamenu__sidebar__link')].forEach(elem => {
						elem.classList.remove('megamenu__sidebar__link--active');
					});
					category.classList.add('megamenu__sidebar__link--active');
				}
			});
		});

		// Mobile MegaMenu-related functions 
		var burgerMenu = document.getElementsByClassName('fa-bars')[0];

		burgerMenu.addEventListener('click', function () {
			document.getElementsByClassName("sidenav")[0].style.width = "85%";
		});

		var closeMobileMenu = document.getElementsByClassName('closebtn')[0];

		closeMobileMenu.addEventListener('click', function () {
			document.getElementsByClassName("sidenav")[0].style.width = "0";
		});

		//========================================================================
		// Event listeners for the Megamenu
		//========================================================================

		// Display - hide eventListener for each sections
		var erp = document.getElementsByClassName('megamenu__erp')[0];
		erp.addEventListener('click', function () { switchSection('ERP-section', erp) });

		var bi = document.getElementsByClassName('megamenu__bi')[0];
		bi.addEventListener('click', function () { switchSection('BI-section', bi) });

		var crm = document.getElementsByClassName('megamenu__crm')[0];
		crm.addEventListener('click', function () { switchSection('CRM-section', crm) });

		var hcm = document.getElementsByClassName('megamenu__hcm')[0];
		hcm.addEventListener('click', function () { switchSection('HCM-section', hcm) });

		var scm = document.getElementsByClassName('megamenu__scm')[0];
		scm.addEventListener('click', function () { switchSection('SCM-section', scm) });

		// Display - hide function for the Megamenu categories
		function switchSection(type) {
			//Select all the sections
			[...document.querySelectorAll('.MM-Sections')].forEach(subElem => {

				//Display the elements from the selected section
				if (subElem.classList.contains('hidden') && subElem.classList.contains(type)) {
					subElem.classList.remove('hidden');
				}
				//Otherwise, hide them 
				else if (!subElem.classList.contains(type)) {
					subElem.classList.add('hidden');
				}
			});
		}


		//========================================================================
		// Event listeners for each of the mobile menu categories
		//========================================================================

		// Display - hide function for the SC category
		var sc = document.getElementsByClassName('SC')[0];
		sc.addEventListener('click', function () {

			menuHandler('SC', sc);

			// If hiding the menu, ensure that the sub-menu are hided as well
			[...document.querySelectorAll('.sub-erp')].forEach(subElem => {
				//Check if hidden 
				if (!subElem.classList.contains('hidden')) {
					//If visible, hide them 	
					subElem.classList.add('hidden');
				}
			});
		});

		// Display - hide function for the SS category
		var ss = document.getElementsByClassName('SS')[0];
		ss.addEventListener('click', function () { menuHandler('SS', ss) });

		// Display - hide function for the RR category
		var rr = document.getElementsByClassName('RR')[0];
		rr.addEventListener('click', function () { menuHandler('RR', rr) });

		// Display - hide function for the categories
		function menuHandler(category, object) {
			if (object.classList.contains('categories--active')) {
				//Display an arrow
				object.classList.add('categories');
				object.classList.remove('categories--active');

				//Show the others categories 
				document.getElementsByClassName('SS')[0].classList.remove('hidden');
				document.getElementsByClassName('SC')[0].classList.remove('hidden');
				document.getElementsByClassName('RR')[0].classList.remove('hidden');
				document.getElementsByClassName('menu-footer')[0].classList.remove('hidden');
			}
			else {
				//Display a reversed arrow
				object.classList.add('categories--active');
				object.classList.remove('categories');

				//Hide the others categories 
				document.getElementsByClassName('SS')[0].classList.add('hidden');
				document.getElementsByClassName('SC')[0].classList.add('hidden');
				document.getElementsByClassName('RR')[0].classList.add('hidden');
				document.getElementsByClassName('menu-footer')[0].classList.add('hidden');

				document.getElementsByClassName(category)[0].classList.remove('hidden');
			}

			var subMenu = '.sub-' + category;

			//Select all of the elements with the specific class
			[...document.querySelectorAll(subMenu)].forEach(subElem => {
				//Check if hidden 
				if (subElem.classList.contains('hidden')) {
					//If so, display them 	
					subElem.classList.remove('hidden');
				}
				//Otherwise, hide them 
				else {
					subElem.classList.add('hidden');
				}
			});
		}

		//========================================================================
		// Event listeners for each of the mobile software categories 
		//========================================================================

		// Display all the elements inside the ERP categories
		var erpMenu = document.getElementsByClassName('ERP')[0];
		erpMenu.addEventListener('click', function () { softwareSwitch('.sub-erp') });

		var biMenu = document.getElementsByClassName('BI')[0];
		biMenu.addEventListener('click', function () { softwareSwitch('.sub-BI') });

		var crmMenu = document.getElementsByClassName('CRM')[0];
		crmMenu.addEventListener('click', function () { softwareSwitch('.sub-CRM') });

		var hcmMenu = document.getElementsByClassName('HCM')[0];
		hcmMenu.addEventListener('click', function () { softwareSwitch('.sub-HCM') });

		var scmMenu = document.getElementsByClassName('SCM')[0];
		scmMenu.addEventListener('click', function () { softwareSwitch('.sub-SCM') });

		function softwareSwitch(type){
			//Select all of the elements with the specific class
			[...document.querySelectorAll(type)].forEach(subElem => {
				//Check if hidden 
				if (subElem.classList.contains('hidden')) {
					//If so, display them 	
					subElem.classList.remove('hidden');
				}
				//Otherwise, hide them 
				else {
					subElem.classList.add('hidden');
				}
			});
		}

	//============================================================================
	// Rest of the proof of concept JS done by Roméo
	//============================================================================
		[...document.querySelectorAll('.js-megamenu-link')].forEach(link => {
			if (this.triggers.get(link.dataset.megamenu) === undefined) {
				this.triggers.set(link.dataset.megamenu, []);
			}

			link.addEventListener('click', this.changeMenuPage);
			this.triggers.get(link.dataset.megamenu).push(link);
		});

		[...document.querySelectorAll('.js-megamenu-page')].forEach(page => {
			this.pages.set(page.dataset.megamenu, page);
		});
	}

	changeMenuPage(event) {
		let link = event.currentTarget;
		if (event.currentTarget.tagName !== 'A') {
			link = Utils.upTo(event.currentTarget, 'a');
		}

		const linkIsAlreadyActive = link.classList.contains('main-nav__left__link--active');

		for (const page of this.pages.values()) {
			page.classList.remove('main-nav__megamenu--active');
		}

		for (const triggers of this.triggers.values()) {
			triggers.forEach(trigger => {
				trigger.classList.remove('main-nav__left__link--active');
			});
		}

		const arrow = document.querySelector('.js-megamenu-arrow');
		if (arrow !== null) {
			arrow.classList.remove('megamenu__arrow--active');
		}

		if (!linkIsAlreadyActive) {
			link.classList.add('main-nav__left__link--active');
			this.pages.get(link.dataset.megamenu).classList.add('main-nav__megamenu--active');
			if (arrow !== null) {
				arrow.classList.add('megamenu__arrow--active');
				arrow.style.left = `${link.offsetLeft + (link.offsetWidth / 2) - (arrow.offsetWidth / 2)}px`;
			}
		}

		event.stopPropagation();
		event.preventDefault();
	}
}
