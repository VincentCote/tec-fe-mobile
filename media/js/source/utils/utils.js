export default class Utils {
	static upTo(el, tagName) {
		tagName = tagName.toLowerCase();

		while (el && el.parentNode) {
			el = el.parentNode;
			if (el.tagName && el.tagName.toLowerCase() === tagName) {
				return el;
			}
		}

		return null;
	}
}
