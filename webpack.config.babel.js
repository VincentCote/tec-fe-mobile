import path from 'path';
import webpack from 'webpack';
import AssetsPlugin from 'assets-webpack-plugin';
import {BundleAnalyzerPlugin} from 'webpack-bundle-analyzer';
import settings from './package';

const isDebug = !process.argv.includes('--release');
const isAnalyze = process.argv.includes('--analyze') || process.argv.includes('--analyse');

const config = {
	output: {
		path: path.resolve(__dirname, 'media/js/'),
		publicPath: '/media/js/',
		filename: isDebug ? '[name].js' : '[name].[chunkhash:8].js',
		chunkFilename: isDebug ? '[name].chunk.js' : '[name].[chunkhash:8].chunk.js'
	},
	entry: ['babel-polyfill', path.resolve('./media/js/source/app.js')],
	optimization: {
		minimize: !isDebug
	},
	mode: isDebug ? 'development' : 'production',
	module: {
		rules: [
			{
				test: /\.js?$/,
				loader: 'babel-loader',
				include: [
					path.resolve(__dirname, 'media/js/source')
				],
				query: {
					// https://github.com/babel/babel-loader#options
					cacheDirectory: isDebug,
					babelrc: false,
					presets: [
						// A Babel preset that can automatically determine the Babel plugins and polyfills
						// https://github.com/babel/babel-preset-env
						['env', {
							targets: {
								browsers: settings.browsers
							},
							modules: false,
							useBuiltIns: false,
							debug: false
						}]
					]
				}
			}
		]
	},
	plugins: [
		// Define free variables
		// https://webpack.github.io/docs/list-of-plugins.html#defineplugin
		new webpack.DefinePlugin({
			'process.env.NODE_ENV': isDebug ? '"development"' : '"production"',
			'process.env.BROWSER': true,
			mlp: (isDebug) ? JSON.stringify(settings.mlp.local_serve) : JSON.stringify('/' + settings.mlp.production),
			__DEV__: isDebug
		}),

		// Emit a file with assets paths
		// https://github.com/sporto/assets-webpack-plugin#options
		new AssetsPlugin({
			path: path.resolve(__dirname),
			filename: 'assets.json',
			fullPath: false,
			prettyPrint: true
		}),

		// Webpack Bundle Analyzer
		// https://github.com/th0r/webpack-bundle-analyzer
		...isAnalyze ? [new BundleAnalyzerPlugin()] : []
	],

	// Choose a developer tool to enhance debugging
	// http://webpack.github.io/docs/configuration.html#devtool
	devtool: isDebug ? 'cheap-module-source-map' : false,

	// Some libraries import Node modules but don't use them in the browser.
	// Tell Webpack to provide empty mocks for them so importing them works.
	// https://webpack.github.io/docs/configuration.html#node
	// https://github.com/webpack/node-libs-browser/tree/master/mock
	node: {
		fs: 'empty',
		net: 'empty',
		tls: 'empty'
	}
};

export default config;
