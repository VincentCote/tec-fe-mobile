'use strict';

import path from 'path';
import gulp from 'gulp';
import git from 'gulp-git';
import util from 'gulp-util';
import less from 'gulp-less';
import pug from 'gulp-pug';
import svgo from 'gulp-svgmin';
import cleanCSS from 'gulp-clean-css';
import autoprefixer from 'gulp-autoprefixer';
import mergeStream from 'merge-stream';
import concat from 'gulp-concat';
import hash from 'gulp-hash';
import del from 'del';
import runSequence from 'run-sequence';
import sftp from 'gulp-sftp';
import webpackStream from 'webpack-stream';
import webpack from 'webpack';
import browserSyncModule from 'browser-sync';
import argvModule from 'yargs';
import pump from 'pump';
import settings from './package';

const browserSync = browserSyncModule.create();
const argv = argvModule.argv;
const isRelease = process.argv.includes('--release');
const isPatch = process.argv.includes('--patch');
process.argv.push('--watch');

// Compiles all pug templates into the GEL folder
// This also checks wether or not the --release CLI argument is
// set, and adds a local accordingly.
// This is useful for adding dev-only dependencies (such as BrowserSync)
gulp.task('pug', (isRelease) ? ['less'] : null, () => {
	// This gets the current commit hash to correctly show it
	// in the GEL Tools

	let gitHash = 'N/A';
	if (settings.gelTools) {
		git.revParse({args: '--short HEAD'}, (err, hash) => {
			if (err) {
				util.log('Error while trying to find git hash.');
			} else {
				gitHash = hash;
			}
		});
	}

	let jsFilename = '';
	let cssFilename = '';
	if (isRelease) {
		const assets = require('./assets');
		jsFilename = assets.main.js;
		cssFilename = assets['master.css'];
	}

	return pump([
		gulp.src([path.resolve('./views/', '*.pug'), path.resolve('./views/pages/**/', '*.pug')]),
		pug({locals: {
			dev: !isRelease,
			gel: argv.gel,
			mlp: (isRelease) ? '/' + settings.mlp.production : settings.mlp.local_serve,
			sha: gitHash,
			jsHash: jsFilename,
			cssHash: cssFilename,
			cb: Math.floor((Math.random() * 10000000000) + 1)
		}}).on('error', handleError),
		gulp.dest((isRelease) ? path.resolve('./export') : path.resolve('./GEL'))
	]);
});

// Reload the browser after the pug task is done.
// This is done by calling this task, which has a pre-requiste
// to run the pug task.
gulp.task('pug-watch', ['pug'], callback => {
	browserSync.reload();
	callback();
});

// Compiles master.less one folder up (outside of the source folder)
gulp.task('less', (isRelease) ? ['js'] : null, () => {
	const sources = [path.resolve('./media/css/source', 'master.less')];

	const vendorCSS = pump([
		gulp.src(['node_modules/normalize.css/normalize.css']),
		concat('vendor.css')
	]);

	const lessTask = pump([
		gulp.src(sources),
		// Dynamically add the mlp variable per environment
		less({
			globalVars: {
				mlp: (isRelease) ? '"/' + settings.mlp.production + '"' : '"' + settings.mlp.local_serve + '"',
				patch: (isRelease && isPatch) ? '_patch' : ''
			}
		}).on('error', handleError),
		autoprefixer()
	]);

	return pump([
		mergeStream(vendorCSS, lessTask),
		concat('master.css'),
		cleanCSS(),
		(isRelease) ? hash({
			template: '<%= name %>.<%= hash %><%= ext %>'
		}) : util.noop(),
		gulp.dest((isRelease) ? path.resolve('./export', settings.mlp.production, 'css/') : path.resolve(settings.mlp.local, 'css/')),
		(isRelease) ? hash.manifest(path.resolve(__dirname, 'assets.json'), true, '  ') : util.noop(),
		(isRelease) ? gulp.dest(__dirname) : util.noop(),
		browserSync.stream()
	]);
});

// Compiles JS into a neat package using webpack.
// See `webpack.config.babel.js` for more informations.
gulp.task('js', () => {
	return pump([
		gulp.src('./media/js/source/app.js'),
		webpackStream(require('./webpack.config.babel.js').default, webpack),
		gulp.dest((isRelease) ? path.resolve('./export', settings.mlp.production, 'js/') : path.resolve(settings.mlp.local, 'js/'))
	]);
});

// Reload the browser after webpack is done.
gulp.task('js-watch', ['js'], callback => {
	browserSync.reload();
	callback();
});

// Simply copy images over
gulp.task('img', callback => {
	if (isRelease) {
		return pump([
			gulp.src(['./media/img/**/*.*']),
			gulp.dest(path.resolve('./export', settings.mlp.production, 'img/'))
		]);
	}

	callback();
});

// Simply copy fonts over
gulp.task('fonts', callback => {
	if (isRelease) {
		return pump([
			gulp.src(['./media/fonts/**/*.*']),
			gulp.dest(path.resolve('./export', settings.mlp.production, 'fonts/'))
		]);
	}

	callback();
});

// Passes SVGs through SVGO, reducing file sizes.
gulp.task('svgo', callback => {
	if (isRelease) {
		return pump([
			gulp.src(['./media/img/*.svg']),
			svgo(settings.svgoConfig),
			gulp.dest(path.resolve('./export', settings.mlp.production, 'img/'))
		]);
	}

	callback();
});

// Quick compilation only task, mainly used by `npm build`
gulp.task('clean', () => {
	return del(['./export']);
});

// Quick compilation only task, mainly used by `npm build`
gulp.task('compile', callback => {
	if (isRelease) {
		runSequence('clean', ['pug', 'less', 'js', 'img', 'fonts', 'svgo'], callback);
	} else {
		runSequence(['pug', 'less', 'js', 'img', 'fonts', 'svgo'], callback);
	}
});

// Compiles and updates the GEL using the settings found in `package.json`
gulp.task('deploy', ['compile'], callback => {
	if (settings.ftp.deploy) {
		util.log('Deploying via sFTP...');

		pump([
			gulp.src(['./export/**'], {buffer: false}),
			sftp({
				host: settings.ftp.host,
				remotePath: settings.ftp.serverPath,
				auth: 'keyMain'
			})
		], callback);
	} else {
		util.log('FTP deploy disabled in', util.colors.cyan('package.json'), '- skipping...');
		callback();
	}
});

// Serve the files via BrowserSync, and watch for changes to any files
gulp.task('serve', ['compile'], () => {
	// Define base BrowserSync options
	const bsOptions = {
		server: {
			baseDir: ['GEL', 'media']
		}
	};

	// If we find a host file, add that to the config object
	if (argv.host === undefined) {
		util.log('Host argument not found or invalid.');
		util.log('Using default network interface...');
	} else {
		util.log('Host argument found: ' + argv.host);
		bsOptions.host = argv.host;
	}

	// Starts BrowserSync
	browserSync.init(bsOptions);

	// Calls `pug-watch`, which in turn calls `pug`, and reloads the page
	gulp.watch('**/*.pug', {cwd: './views/'}, ['pug-watch']);

	// Calls the less task
	gulp.watch('**/*.less', {cwd: './media/css/source/'}, ['less']);

	// Reload the page if any of the JS file changes
	gulp.watch('**/*.js', {cwd: './media/js/source/'}, ['js-watch']);
});

function handleError(err) {
	console.error(err.message);
	this.emit('end');
}
